const constants = require('./constants')
const utils = require('./utils')

const sprintf = require('sprintf-js').sprintf
const URL = require('url').URL
const http = require('http')

// Internal state
const cache = {};

// Helpers
function get_cat_image(code) {
    return new URL("/page-assets/cats/" + code + ".jpg", constants.CDN_PREFIX);
}

// Generates html error page for the specified code.
module.exports = function (req, res, mob, code) {
    // Build response.
    res.writeHead(code, { 'Content-Type': 'text/html' });
    // Check cache.
    if (code in cache) {
        return cache[code];
    }
    // Create base html.
    res.writeHead(code, { 'Content-Type': 'text/html' });
    dom = utils.get_default_page(mob);
    document = dom.window.document;
    utils.add_css(document, constants.COMMON_CSS);
    if (code in http.STATUS_CODES) {
        document.title = sprintf("Error %d: %s!", code, http.STATUS_CODES[code]);
    }
    else {
        document.title = sprintf("Error %d: Unknown error!", code);
    }
    // Build webpage components.
    var root = document.getElementsByTagName("body")[0];
    var constraint = document.createElement("div");
    constraint.classList.add("centerscreen");
    var cat = document.createElement("img");
    cat.style.width = "96%";
    cat.style.maxWidth = "750px";
    cat.style.height = "auto";
    cat.alt = document.title;
    cat.src = get_cat_image(code);
    // Layout website.
    constraint.appendChild(cat);
    root.appendChild(constraint);
    // Finish up and add to the cache.
    console.log("Adding error page %d to the cache.", code);
    var final = utils.dom2html(dom);
    cache[code] = final;
    return final;
}