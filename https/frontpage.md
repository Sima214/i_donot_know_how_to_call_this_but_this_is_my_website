# Anastasios Simeonidis

## Who am I

Currently I am an undergraduate student of [School of Informatics of the Faculty of Sciences of the Aristotle University of Thessaloniki](https://www.csd.auth.gr/en/school/about).

## Content

You are at the home page of a website under construction. This is where some software development guides and articles about how my and other people's software works is supposed to go. Currently, as the server is still in development, there is not much here.

## Current projects I am working on

1. [SS-CE](sima214.me/ssce)
2. [mywebsite](sima214.me)
3. [Esonov](sima214.me/esonov)
4. [Genume](https://foss.csd.auth.gr)

## Navigation

TODO: There is supposed to be a navigation bar on the left...

## Contact

[![Github profile link](https://d3f3nimbmr2mxp.cloudfront.net/page-assets/github_mark.png)](https://github.com/Sima214)