const gen_errorpage = require('./error-gen')
const gen_front = require('./front')
const constants = require('./constants')
const utils = require('./utils')

const MobileDetect = require('mobile-detect')

// Main server response loop
module.exports = async function (request, response) {
    if (request.method === "OPTIONS") {
        // Cors configuration.
        var headers = {};
        headers["Access-Control-Allow-Origin"] = '*';
        headers["Access-Control-Allow-Methods"] = "GET, HEAD, OPTIONS";
        headers["Access-Control-Allow-Credentials"] = true;
        // 24 hours
        headers["Access-Control-Max-Age"] = '86400';
        headers["Access-Control-Allow-Headers"] = "Origin, X-Requested-With, Content-Type, Accept";
        response.writeHead(200, headers);
        response.end();
    }
    else if (request.method === 'GET' || request.method === 'HEAD') {
        // Valid request, continue parsing.
        mobile = new MobileDetect(request.headers['user-agent']).mobile() != null;
        body = request.method === 'GET';
        // Split url to find the requested path.
        path = request.url.split('/').slice(1);
        switch (path[0]) {
            case '':
                // Front
                var page = await gen_front(request, response, mobile);
                utils.finish_request(response, body, page);
                break;
            case 'teapot':
                // Aprif fools
                var page = gen_errorpage(request, response, mobile, 418);
                utils.finish_request(response, body, page);
                break;
            default:
                // Not found
                var page = gen_errorpage(request, response, mobile, 404);
                utils.finish_request(response, body, page);
        }
        response.end();
    }
    else {
        // Invalid request
        console.log("Invalid request: %s from %s", request.method, request.connection.remoteAddress);
        var page = gen_errorpage(request, response, mobile, 405);
        utils.finish_request(response, body, page);
        response.end();
    }
}