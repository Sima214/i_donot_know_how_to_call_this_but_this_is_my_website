const constants = require('./constants')
const utils = require('./utils')
const gen_errorpage = require('./error-gen')

const sprintf = require('sprintf-js').sprintf
const fs = require('fs')

var cached = null;
var cached_time = null;

// Generates the main page(/).
module.exports = async function (req, res, mob) {
    // Check if file exists.
    if (!fs.existsSync(constants.FRONTPAGE_SRC)) {
        return gen_errorpage(req, res, mob, 404);
    }
    // Build response headers.
    res.writeHead(200, {
        'Content-Type': 'text/html',
        'Cache-Control': sprintf('max-age=%d', constants.HTML_MAX_AGE)
    });
    last_time = fs.statSync(constants.FRONTPAGE_SRC).mtimeMs;
    // Check cache.
    if (cached != null && last_time === cached_time) {
        return cached;
    }
    // Start conversion process.
    var content_promise = utils.md2html(constants.FRONTPAGE_SRC);
    // Create base html.
    dom = utils.get_default_page(mob);
    document = dom.window.document;
    document.title = "Sima214, Computer Science Student"
    utils.add_css(document, constants.ROBOTO_CSS);
    utils.add_css(document, constants.COMMON_CSS);
    // Build final page.
    var root = document.getElementsByTagName("body")[0];
    root.innerHTML = await content_promise;
    // Finish up and cache.
    console.log("Cached front page!");
    var final = utils.dom2html(dom);
    cached = final;
    cached_time = last_time;
    return final;
}