const constants = require('./constants')
const main = require('./main')

const http = require('http')

// Server initialization
const server = http.createServer(main);
server.listen(constants.SERVER_PORT, constants.SERVER_HOSTNAME);
console.log("Nodejs server listening to %s:%d", constants.SERVER_HOSTNAME, constants.SERVER_PORT)
