const constants = require('./constants')

const sprintf = require('sprintf-js').sprintf
const pandoc = require('node-pandoc');
const htmlmin = require('html-minifier').minify
const jsdom = require('jsdom')
const { JSDOM } = jsdom;

module.exports = {
    finish_request: function (responce, req_body, body) {
        if (req_body) {
            responce.write(body);
        }
    },
    get_default_page: function (mobile) {
        return new JSDOM(constants.HTML5_BASE_MOBILE);
    },
    minify: function (input) {
        return htmlmin(input, {
            removeComments: true,
            removeCommentsFromCDATA: true,
            removeCDATASectionsFromCDATA: true,
            collapseWhitespace: true,
            conservativeCollapse: false,
            collapseBooleanAttributes: false,
            removeAttributeQuotes: false,
            removeRedundantAttributes: false,
            useShortDoctype: true,
            removeEmptyAttributes: false,
            removeOptionalTags: false,
            removeEmptyElements: false,
            lint: false,
            keepClosingSlash: false,
            caseSensitive: false,
            minifyJS: false,
            minifyCSS: true,
            ignoreCustomComments: [],
            processScripts: []
        })
    },
    dom2html: function (document) {
        var base = document.window.document.getElementsByTagName('html')[0].innerHTML;
        var fixed = sprintf(constants.HTML5_FIXUP, base);
        return module.exports.minify(fixed);
    },
    add_css: function (document, csslink) {
        var head = document.getElementsByTagName('head')[0];
        var link = document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = csslink;
        link.media = 'all';
        head.appendChild(link);
    },
    md2html: async function (src) {
        var waiter = new Promise((resolve, reject) => {
            var done = false;
            pandoc(src, "-f markdown -t html", (err, res) => {
                if (err) {
                    console.error("Could not convert %s to html!", src);
                    console.error(err);
                    if (!done) {
                        done = true;
                        reject(err);
                    }
                } else if (!done) {
                    console.log("Successfully converted %s to html!", src);
                    done = true;
                    resolve(res);
                }
            });
        })
        return waiter;
    }
}